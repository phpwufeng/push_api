<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Messqge extends Model
{
    use HasFactory;
    protected $table = 'message'; // 在这指定一个表名
    public $timestamps = false;
}

<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    private static $key = '[push_api_2022.7.24]';
    public function input($post, $parameters, &$result)
    {
        //array_push($parameters,'SIGN', 'sign');
        if (env('s_maintain'))
        {
            $result = [
                'code' => 900,
                'msg' => "服务器正在维护…",
            ];
            return null;
        }
        else
        {
            $data = json_decode($post, true);
            $is_complete = true;
            foreach ($parameters as $v)
            {
                if (!isset($data[$v]))
                {
                    $is_complete = false;
                    break;
                }
            }
            if (!$is_complete)
            {
                $result = [
                    'code' => -1,
                    'msg' => "缺少参数",
                ];
                $data = null;
            }else
            {
                $result = [
                    'code' => 1,
                    'msg' => "success",
                ];
            }
        }
        return $data;
    }

    function send_get($url,$data){
        $url .= "?a=1";
        if(is_array($data)){
            foreach ($data as $key => $value){
                $url .= "&".$key."=".$value;
            }
        }
        $oCurl = curl_init();
        if(stripos($url,"https://")!==FALSE){
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, FALSE);
            curl_setopt($oCurl, CURLOPT_SSLVERSION, 1); //CURL_SSLVERSION_TLSv1
        }
        curl_setopt($oCurl, CURLOPT_URL, $url);//目标URL
        curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1 );//设定是否显示头信息,1为显示
        curl_setopt($oCurl, CURLOPT_BINARYTRANSFER, true) ;//在启用CURLOPT_RETURNTRANSFER时候将获取数据返回
        $sContent = curl_exec($oCurl);
        $aStatus = curl_getinfo($oCurl);//获取页面各种信息
        curl_close($oCurl);
        if(intval($aStatus["http_code"])==200){
            $sContent = json_decode($sContent, true);
            // 转成json数组形式
            return $sContent;
        }else{
            return false;
        }
    }

    function send_post($url,$postDataArr){
        $postdata = json_encode($postDataArr);
        $ch = curl_init();    // 启动一个CURL会话
        curl_setopt($ch, CURLOPT_URL, $url);     // 要访问的地址
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  // 对认证证书来源的检查   // https请求 不验证证书和hosts
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);  // 从证书中检查SSL加密算法是否存在
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
        curl_setopt($ch, CURLOPT_POST, true); // 发送一个常规的Post请求
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);     // Post提交的数据包
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);     // 设置超时限制防止死循环
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        //curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     // 获取的信息以文件流的形式返回
        $result = curl_exec($ch);

        // 打印请求的header信息
        //$a = curl_getinfo($ch);
        //var_dump($a);

        curl_close($ch);
        $result = json_decode($result, true);
        return $result;

    }




}

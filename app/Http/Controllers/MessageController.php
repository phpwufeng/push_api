<?php

namespace App\Http\Controllers;

use App\Models\Messqge;
use App\Models\MessqgeRead;
use App\Models\Token;
use App\Models\User;
use Illuminate\Http\Request;

class MessageController extends Controller
{

    /**
     * 消息推送
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function pushMessage(Request $request)
    {
        $data = $request->getContent() ?? "";
        $result = [];
        $parameters =  ["FID","FTITLE","DESC","FCONTENT","FUSER","FPHONE","FHEAD","FENTER","FCREATER","FCREATETIME"];
        $data_result = $this->input($data, $parameters, $result);
        if ($data_result == null)
        {
            echo json_encode($result);
            die;
        }
        $data_info = json_decode($data, true);
        $title = $data_info['FTITLE'] ?? "";
        $content = $data_info['FCONTENT'] ?? "";
        $user = $data_info['FUSER'] ?? "";
        $phone = $data_info['FPHONE'] ?? "";
        $DESC = $data_info['DESC'] ?? "";
        $FID = intval($data_info['FID']) ?? 0;
        $create_time = strtotime($data_info['FCREATETIME']) ?? time();
        //入库
        $id = Messqge::insertGetId([
                'title' => $title,
                'content' => json_encode($content),
                'user' => $user,
                'desc' => $DESC,
                'phone' => $phone,
                'created_at' => time(),
                'FHEAD' => json_encode($data_info['FHEAD']) ?? "",
                'FENTER' => json_encode($data_info['FENTER']) ?? "",
                'FCREATER' => $data_info['FCREATER'] ?? "",
                'FCREATETIME' => $create_time,
                'FID' => $FID
            ]);
        //推送微信
        if($id > 0) {
            //获取access_token
            $access_token_info = Token::where("id",1)->where('time', '>=', time())->first();
            if($access_token_info){
                $access_token = $access_token_info['access_token'];
            }else {
                $data_get['corpid'] = env('corpid');
                $data_get['corpsecret'] = env('corpsecret');
                $access_token_info = $this->send_get("https://qyapi.weixin.qq.com/cgi-bin/gettoken", $data_get);
                if($access_token_info['errcode'] > 0 ){
                    $result = [
                        'code' => -200,
                        'msg' => $access_token_info['errmsg'],
                    ];
                    return $result;
                }
                $access_token = $access_token_info['access_token'];
                $expires_in = $access_token_info['expires_in'];
                $udata['time'] = time() + $expires_in;
                $udata['access_token'] = $access_token;
                Token::where("id",1)->update($udata);
            }
            //查询user_id
//            $user_info = User::where("phone",$phone)->first();
//            if($user_info){
//                $user_id = $user_info['user_id'];
//            }else {
                $phone_data_post['mobile'] = $phone;
                $user_result = $this->send_post("https://qyapi.weixin.qq.com/cgi-bin/user/getuserid?access_token=" . $access_token, $phone_data_post);
                if($user_result['errcode'] > 0 ){
                    $result = [
                        'code' => -200,
                        'msg' => $user_result['errmsg'],
                    ];
                    return response()->json($result);
                }else {
                    $user_id = $user_result['userid'];
                    //插入数据库
                    $add_user_data['user_id'] = $user_id;
                    $add_user_data['phone'] = $phone;
                    $add_user_data['updated_at'] = time();
                    User::insert($add_user_data);
                }
//            }
            //消息推送
            $url = "http://vxgrhc.wxtcxx.com:8083?message_id=".$id;
            $data_post['touser'] = $user_id;
            $data_post['toparty'] = "";
            $data_post['totag'] = "";
            $data_post['msgtype'] = "textcard";
            $data_post['agentid'] = env('agentid');
            $data_post['textcard']['title'] = $title;
            $data_post['textcard']['description'] = $DESC;
            $data_post['textcard']['url'] = $url;
            $data_post['textcard']['btntxt'] = '详情';
            $data_post['enable_id_trans'] = 0;
            $data_post['enable_duplicate_check'] = 0;
            $data_post['duplicate_check_interval'] = 1800;
            $push_result = $this->send_post("https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=".$access_token, $data_post);
            if($push_result['errcode'] > 0 ){
                $result = [
                    'code' => -200,
                    'msg' => $push_result['errmsg'],
                ];
            }else {
                //推送成功，修改数据库
                Messqge::where('id', $id)->update([
                    'status' => 1,
                ]);
            }
        }
        return response()->json($result);
    }

    public function readMessage(Request $request)
    {
        $data = $request->getContent() ?? "";
        $result = [];
        $parameters =  ['message_id'];
        $data_result = $this->input($data, $parameters, $result);
        if ($data_result == null)
        {
            echo json_encode($result);
            die;
        }
        $data_info = json_decode($data, true);
        $message_id = $data_info['message_id'] ?? "";
        $mess_info = Messqge::where('id', $message_id)
            ->select("title as FTITLE","desc as DESC","content as FCONTENT","user as FUSER","phone as FPHONE","FHEAD","FENTER","FCREATER","created_at as FSENDTIME","updated_at as FREADTIME","FCREATETIME","status as FSTATUS")
            ->first();
        $mess_info['FCREATETIME'] = $mess_info['FCREATETIME'] - 8*3600;

        //修改库状态
        if($mess_info && $mess_info['FSTATUS'] == 1) {
            Messqge::where('id', $message_id)->update([
                'status' => 2,
                'updated_at' => time(),
            ]);
        }
        if($this->isJson($mess_info['FCONTENT'])) {
            $mess_info['FCONTENT'] = json_decode($mess_info['FCONTENT'], true);
        }
        if($this->isJson($mess_info['FHEAD'])) {
            $mess_info['FHEAD'] = json_decode($mess_info['FHEAD'], true);
        }
        if($this->isJson($mess_info['FENTER'])) {
            $mess_info['FENTER'] = json_decode($mess_info['FENTER'], true);
        }

        $result['info'] = $mess_info;
        return response()->json($result);
    }

    public function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

}

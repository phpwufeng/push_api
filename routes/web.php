<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    echo "success";
});
use App\Http\Controllers\MessageController;
Route::post('/push_message', [MessageController::class, 'pushMessage']);
Route::post('/api/read_message', [MessageController::class, 'readMessage']);